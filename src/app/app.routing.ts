import { Routes, RouterModule } from '@angular/router';
import {PresentationComponent} from './presentation/presentation.component';
import {TeamComponent} from './team/team.component';
import {HomeComponent} from './home/home.component';
import {OurServicesComponent } from './our-services/our-services.component'
import {ContactComponent } from './contact/contact.component'

const APP_ROUTES: Routes = [
    {path: '', redirectTo: 'home', pathMatch: 'full'},
    {path: 'home', component: HomeComponent},
    {path: 'team', component: TeamComponent},
    {path: 'services', component: OurServicesComponent},
    {path: 'contact', component: ContactComponent}
];

export const routing = RouterModule.forRoot(APP_ROUTES);