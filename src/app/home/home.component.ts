import { Component, OnInit, trigger, state, transition, animate, style } from '@angular/core';

@Component({
  selector: 'prism-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']})
export class HomeComponent implements OnInit {
  public mainImage: string = './assets/images/side-image-bw.jpg';
  constructor() { }

  ngOnInit() {
  }

}
