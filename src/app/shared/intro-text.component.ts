import { Component, OnInit, trigger, state, transition, animate, style } from '@angular/core';

@Component({
  selector: 'prism-intro-text',
  templateUrl: './intro-text.component.html',
  styleUrls: ['./intro-text.component.css'],
 animations:[
    trigger('divState', [
      state('void', style({
        opacity: 0,
        transform: 'translateY(9px)',
      })),
      state('shown', style({
        opacity: 1,
        transform: 'translateY(0)',
      })),
      transition('void => *', animate(1000)),
    ]),
    trigger('divState2', [
      state('void', style({
        opacity: 0,
        transform: 'translateY(13px)',
      })),
      state('shown', style({
        opacity: 1,
        transform: 'translateY(0)',
      })),
      transition('void => *', animate(1350)),
    ])
  ]
})
export class IntroTextComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
