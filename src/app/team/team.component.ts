import { Component, OnInit, trigger, state, transition, animate, style } from '@angular/core';

@Component({
  selector: 'prism-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css']})
export class TeamComponent implements OnInit {
  public mainImage: string = './assets/images/working-dark-room1.jpg';
  constructor() { }

  ngOnInit() {
  }

}
