import { Component, OnInit, trigger, state, transition, animate, style, Input } from '@angular/core';

@Component({
  selector: 'prism-presentation',
  templateUrl: './presentation.component.html',
  styleUrls: ['./presentation.component.css'],
  animations:[
    trigger('divState', [
      state('void', style({
        opacity: 0,
      })),
      state('shown', style({
        opacity: 1,
      })),
      transition('void => *', animate(2750)),
    ])
  ]
})
export class PresentationComponent implements OnInit {

  @Input() mainImage: string;
  constructor() { }

  ngOnInit() {
  }

}
