import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'prism-our-services',
  templateUrl: './our-services.component.html',
  styleUrls: ['./our-services.component.css']
})
export class OurServicesComponent implements OnInit {
  public mainImage: string = '../assets/images/working-dark-room1.jpg';
  constructor() { }

  ngOnInit() {
  }

}
