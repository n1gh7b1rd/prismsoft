import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'prism-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  
  open: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  toggle(){
    this.open = !this.open;
  }

}
