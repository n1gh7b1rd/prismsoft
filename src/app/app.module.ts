import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {routing} from './app.routing';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { LogoComponent } from './header/logo.component';
import { PresentationComponent } from './presentation/presentation.component';
import { TeamComponent } from './team/team.component';
import { OurServicesComponent } from './our-services/our-services.component';
import { ContactComponent } from './contact/contact.component';
import { IntroTextComponent } from './shared/intro-text.component';
import { MenuComponent } from './header/menu.component';
import {HomeComponent} from './home/home.component';
import { CardInfoComponent } from './shared/card-info/card-info.component';
import { SpaCardInfoComponent } from './shared/card-info/spa-card-info/spa-card-info.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LogoComponent,
    PresentationComponent,
    TeamComponent,
    OurServicesComponent,
    ContactComponent,
    IntroTextComponent,
    MenuComponent,
    HomeComponent,
    CardInfoComponent,
    SpaCardInfoComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
