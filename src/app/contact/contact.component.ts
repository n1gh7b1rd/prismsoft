import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'prism-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  public mainImage: string = './assets/images/working-dark-room1.jpg';
  constructor() { }

  ngOnInit() {
    
  }

}
