import { PrismsoftPage } from './app.po';

describe('prismsoft App', function() {
  let page: PrismsoftPage;

  beforeEach(() => {
    page = new PrismsoftPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('prism works!');
  });
});
